#include "std_system.h"
#include "stm32f030x6.h"
#include "color.h"
//
typedef enum
{
	const_color = 0,
	green_test,
	rainbow,
	random_color_flash,
	flame,
	rainbow_fast,
	rainbow_step,
	polar_light,
	random_color_fade,
	last_entry_counter,
} type_of_operation_t;
//
void main(void);
void project_def_handler(void);
void sys_init(void);
void TIM17_IRQHandler(void);
void TIM1_CC_IRQHandler(void);
uint16_t get_random_h(void);
void operation_button(void);
void HSV_to_RGB_test(hsv_t* hsv, rgb_888_t* rgb);
//
volatile uint_fast8_t ir_rx_req = 0;
volatile uint32_t ir_rx_data = 0;
volatile hsv_t global_hsv;
volatile uint32_t type_of_operation = const_color;
volatile uint_fast8_t cur_speed = 5, max_v = 0, curr_v = 0;
const uint16_t speed[10] = {479, 239, 159, 119, 95, 79, 68, 59, 52, 47};//10-100
volatile const version_t version __attribute__ ((section (".version"))) =
{
	.number = 0,
	.day = 0x08,
	.month = 0x05,
	.year = 0x20,
};
uint16_t pwm_linear[2][256] =
{{0},
{
	    0,     2,     5,    11,    19,    29,    40,    54,    70,    88,   107,   129,   153,   179,   207,   237,
	  268,   302,   338,   376,   416,   458,   502,   548,   596,   646,   698,   751,   807,   865,   925,   987,
	 1051,  1117,  1186,  1256,  1328,  1402,  1478,  1556,  1636,  1718,  1802,  1888,  1976,  2067,  2159,  2253,
	 2349,  2447,  2548,  2650,  2754,  2860,  2968,  3079,  3191,  3305,  3422,  3540,  3660,  3782,  3907,  4033,
	 4161,  4292,  4424,  4559,  4695,  4833,  4974,  5116,  5261,  5407,  5555,  5706,  5858,  6013,  6169,  6328,
	 6488,  6651,  6815,  6982,  7150,  7321,  7494,  7668,  7845,  8023,  8204,  8387,  8571,  8758,  8947,  9137,
	 9330,  9525,  9721,  9920, 10121, 10323, 10528, 10735, 10944, 11154, 11367, 11582, 11799, 12018, 12238, 12461,
	12686, 12913, 13142, 13373, 13605, 13840, 14077, 14316, 14557, 14800, 15045, 15292, 15541, 15792, 16045, 16300,
	16557, 16816, 17077, 17340, 17605, 17872, 18141, 18412, 18685, 18960, 19237, 19516, 19798, 20081, 20366, 20653,
	20942, 21233, 21527, 21822, 22119, 22418, 22719, 23023, 23328, 23635, 23944, 24256, 24569, 24884, 25202, 25521,
	25842, 26166, 26491, 26818, 27148, 27479, 27812, 28148, 28485, 28825, 29166, 29509, 29855, 30202, 30552, 30903,
	31257, 31612, 31970, 32329, 32691, 33054, 33420, 33788, 34157, 34529, 34902, 35278, 35656, 36035, 36417, 36800,
	37186, 37574, 37963, 38355, 38749, 39145, 39542, 39942, 40344, 40747, 41153, 41561, 41971, 42383, 42796, 43212,
	43630, 44050, 44472, 44895, 45321, 45749, 46179, 46611, 47045, 47481, 47919, 48359, 48801, 49245, 49690, 50138,
	50588, 51040, 51494, 51950, 52408, 52869, 53331, 53795, 54261, 54729, 55199, 55671, 56145, 56621, 57099, 57579,
	58062, 58546, 59032, 59520, 60010, 60502, 60997, 61493, 61991, 62491, 62994, 63498, 64004, 64512, 65023, 65535,
}};
//
void main(void)
{
	global_hsv.raw = 0;
	uint8_t temp_v = 255;
	for(uint16_t i=0; i<256; i++) pwm_linear[0][i] = 257*i;
	type_of_operation_t type_of_operation_prev = const_color;
	//a5 - led_gpio, a9 - ir_in, a13,14 - swd,
	//a6 - pwm_ch1_green, a7 - pwm_ch2_red, b1 - pwm_ch4_blue
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN|RCC_AHBENR_GPIOBEN;
	GPIOA->MODER = 0x2808a400;
	GPIOA->OSPEEDR = 0xffffffff;
	GPIOA->AFR[0] = 0x11000000;
	GPIOA->AFR[1] = 0x20;
	GPIOB->MODER = 8;
	GPIOB->OSPEEDR = 0xc;
	GPIOB->AFR[0] = 0x10;
	//rng = adc+crc
	RCC->AHBENR |= RCC_AHBENR_CRCEN;
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	RCC->CR2 |= RCC_CR2_HSI14ON;
	while((RCC->CR2 & RCC_CR2_HSI14RDY) == 0){};
	if((ADC1->CR & ADC_CR_ADEN) != 0) ADC1->CR &= ~ADC_CR_ADEN;
	ADC1->CR |= ADC_CR_ADCAL;
	while((ADC1->CR & ADC_CR_ADCAL) != 0){};
	do
	{
		ADC1->CR |= ADC_CR_ADEN;
	}
	while ((ADC1->ISR & ADC_ISR_ADRDY) == 0);
	ADC1->CHSELR = ADC_CHSELR_CHSEL16;
	ADC1->SMPR |= ADC_SMPR_SMP_0|ADC_SMPR_SMP_1|ADC_SMPR_SMP_2;
	ADC->CCR |= ADC_CCR_TSEN;
	//tim3 - led pwm
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	TIM3->ARR = 0xffff;
	TIM3->PSC = 19;//9
	TIM3->CCMR1 = 0x6060;
	TIM3->CCMR2 = 0x6000;
	TIM3->CCR1 = 0;
	TIM3->CCR2 = 0;
	TIM3->CCR4 = 0;
	TIM3->CCER = TIM_CCER_CC1E|TIM_CCER_CC2E|TIM_CCER_CC4E;
	TIM3->CR1 = TIM_CR1_CEN|TIM_CR1_ARPE;
	//tim1 - cnt 1us - ir
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	TIM1->PSC = 47;
	TIM1->CCMR1 = 2|(1<<8);
	TIM1->CCER = TIM_CCER_CC2P|TIM_CCER_CC2E|TIM_CCER_CC1E;
	TIM1->SMCR = (6<<4)|4;
	TIM1->DIER = TIM_DIER_CC1IE;
	TIM1->CR1 = TIM_CR1_CEN;
	//tim17 - main 60 Hz
	RCC->APB2ENR |= RCC_APB2ENR_TIM17EN;
	TIM17->ARR = speed[cur_speed];
	TIM17->PSC = 9999;
	TIM17->DIER = TIM_DIER_UIE;
	TIM17->CR1 = TIM_CR1_CEN|TIM_CR1_ARPE;
	//nvic
	NVIC_SetPriority(TIM17_IRQn, 2);//low
	NVIC_SetPriority(TIM1_CC_IRQn, 1);//high
	NVIC_EnableIRQ(TIM1_CC_IRQn);
	NVIC_EnableIRQ(TIM17_IRQn);
	while(1)
	{
		if(ir_rx_req)
		{
			uint8_t bytes[2];
			switch(ir_rx_req)
			{
				case 1:
					bytes[0] = ir_rx_data >> 24;
					bytes[1] = (ir_rx_data >> 16) & 0xff;
					if ((bytes[0] ^ bytes[1]) == 0xff)
					{
						switch (bytes[0])
						{
							case 0xff://up
								if(max_v < 255) max_v++;
							break;
							case 0xfe://down
								if(max_v) max_v--;
							break;
							case 0xfd://off
								type_of_operation_prev = type_of_operation;
								type_of_operation = const_color;
								if(max_v != 0) temp_v = max_v;
								max_v = 0;
							break;
							case 0xfc://on
								type_of_operation = type_of_operation_prev;
								max_v = temp_v;
							break;
							case 0xfb://r
								type_of_operation = const_color;
								global_hsv.color.h = 0;
								global_hsv.color.s = 255;
							break;
							case 0xfa://g
								type_of_operation = const_color;
								global_hsv.color.h = 128;
								global_hsv.color.s = 255;
							break;
							case 0xf9://b
								type_of_operation = const_color;
								global_hsv.color.h = 256;
								global_hsv.color.s = 255;
							break;
							case 0xf8://w
								type_of_operation = const_color;
								global_hsv.color.h = 0;
								global_hsv.color.s = 0;
							break;
							case 0xf7://color_11
								type_of_operation = const_color;
								global_hsv.color.h = 26;
								global_hsv.color.s = 255;
							break;
							case 0xf6://color_12
								type_of_operation = const_color;
								global_hsv.color.h = 154;
								global_hsv.color.s = 255;
							break;
							case 0xf5://color_13
								type_of_operation = const_color;
								global_hsv.color.h = 282;
								global_hsv.color.s = 255;
							break;
							case 0xf4://flash
								if(type_of_operation < (last_entry_counter-1))
								{
									type_of_operation++;
									operation_button();
								}
							break;
							case 0xf3://color_21
								type_of_operation = const_color;
								global_hsv.color.h = 51;
								global_hsv.color.s = 255;
							break;
							case 0xf2://color_22
								type_of_operation = const_color;
								global_hsv.color.h = 179;
								global_hsv.color.s = 255;
							break;
							case 0xf1://color_23
								type_of_operation = const_color;
								global_hsv.color.h = 307;
								global_hsv.color.s = 255;
							break;
							case 0xf0://strobe
								if(type_of_operation > 1)
								{
									type_of_operation--;
									operation_button();
								}
							break;
							case 0xef://color_31
								type_of_operation = const_color;
								global_hsv.color.h = 77;
								global_hsv.color.s = 255;
							break;
							case 0xee://color_32
								type_of_operation = const_color;
								global_hsv.color.h = 205;
								global_hsv.color.s = 255;
							break;
							case 0xed://color_33
								type_of_operation = const_color;
								global_hsv.color.h = 333;
								global_hsv.color.s = 255;
							break;
							case 0xec://fade
								if(cur_speed < 9)
								{
									cur_speed++;
									TIM17->ARR = speed[cur_speed];
								}
							break;
							case 0xeb://color_41
								type_of_operation = const_color;
								global_hsv.color.h = 102;
								global_hsv.color.s = 255;
							break;
							case 0xea://color_42
								type_of_operation = const_color;
								global_hsv.color.h = 230;
								global_hsv.color.s = 255;
							break;
							case 0xe9://color_43
								type_of_operation = const_color;
								global_hsv.color.h = 358;
								global_hsv.color.s = 255;
							break;
							case 0xe8://smooth
								if(cur_speed > 0)
								{
									cur_speed--;
									TIM17->ARR = speed[cur_speed];
								}
							break;
							default: break;
						}
					}
				break;
				default: break;
			}
			ir_rx_req = 0;
		}
	}
}
void project_def_handler(void)
{
}
void sys_init(void)
{
	//all clk = 48MHz
 	FLASH->ACR = 1;
	RCC->CFGR = 10<<18;//48/(HSI/2) = 12-2 = 10, HSI = 8
	RCC->CR |= RCC_CR_PLLON;
	while ((RCC->CR & RCC_CR_PLLRDY) != RCC_CR_PLLRDY){};
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL){};
}
void TIM1_CC_IRQHandler(void)
{
	static uint32_t loc_temp = 0;
	static uint8_t counter = 0;
	static uint8_t state = 0;
	TIM1->SR = 0;
	uint16_t period = TIM1->CCR2;
	uint16_t duty = TIM1->CCR1;
	if(state)
	{
		if((period > 1100)&&(duty > 550)&&(period < 2000))// '0'
		{
			loc_temp >>= 1;
			counter++;
		}
		else
		{
			if((period > 2000)&&(duty > 550)&&(period < 3000))// '1'
			{
				loc_temp = 0x80000000|(loc_temp >> 1);
				counter++;
			}
		}
		if(counter > 31)
		{
			counter = 0;
			ir_rx_data = loc_temp;
			loc_temp = 0;
			ir_rx_req = 1;
			state = 0;
		}
	}
	else
	{
		if((period > 13000)&&(duty > 8000)) state = 1;
		else
		{
			if((period > 11000)&&(duty > 8000)) ir_rx_req = 2;
		}
	}
}
void TIM17_IRQHandler(void)
{
	uint8_t temp_loc;
	static uint8_t direction_h = 0;
	static uint8_t direction_v = 0;
	static uint32_t counter = 0;
	rgb_888_t loc_temp_1;
	TIM17->SR = 0;
	if(counter < 59) counter++;
	else counter = 0;
	switch(type_of_operation)
	{
		case green_test:
			global_hsv.color.h = 120;
			if(curr_v == max_v) direction_v = 1;
			if(curr_v == 0) direction_v = 0;
			if(direction_v) curr_v--;
			else curr_v++;
		break;
		case rainbow_fast:
			global_hsv.color.h++;
			curr_v = max_v;
		break;
		case rainbow:
			if(counter & 1)global_hsv.color.h++;
			curr_v = max_v;
		break;
		case const_color:
			curr_v = max_v;
		break;
		case random_color_flash:
			switch(counter % 60)
			{
				case 0:
					global_hsv.color.s = 0;
					curr_v = 0;
				break;
				case 30:
					global_hsv.color.s = 255;
					curr_v = max_v;
					global_hsv.color.h = get_random_h();
				break;
				default: break;
			}
		break;
		case rainbow_step:
			if((counter % 30) == 0) global_hsv.color.h += 20;
			curr_v = max_v;
		break;
		case flame:
			if(max_v > 10) temp_loc = max_v - 10;
			else temp_loc = 0;
			//if((counter % 10) == 0)
			{
				if(global_hsv.color.h == 13) direction_h = 1;
				if(global_hsv.color.h == 0) direction_h = 0;
				if(direction_h) global_hsv.color.h--;
				else global_hsv.color.h++;
				if(curr_v == max_v) direction_v = 1;
				if(curr_v == temp_loc) direction_v = 0;
				if(direction_v) curr_v--;
				else curr_v++;
			}
		break;
		case polar_light:
			if((counter % 10) == 0)
			{
				if(global_hsv.color.h == 200) direction_h = 1;
				if(global_hsv.color.h == 150) direction_h = 0;
				if(direction_h) global_hsv.color.h--;
				else global_hsv.color.h++;
				if(curr_v == max_v) direction_v = 1;
				if(curr_v == (max_v>>1)) direction_v = 0;
				if(direction_v) curr_v--;
				else curr_v++;
			}
		break;
		case random_color_fade:
			if(curr_v == max_v) direction_v = 1;
			if(curr_v == 0)
			{
				direction_v = 0;
				global_hsv.color.h = get_random_h();
			}
			if(direction_v) curr_v--;
			else curr_v++;

		break;
		default: break;
	}
	global_hsv.color.v = curr_v;
	HSV_to_RGB_test(&global_hsv, &loc_temp_1);
/*
	TIM3->CCR1 = loc_temp_1.color.g;
	TIM3->CCR2 = loc_temp_1.color.r;
	TIM3->CCR4 = loc_temp_1.color.b;
*/
	TIM3->CCR1 = pwm_linear[0][loc_temp_1.color.g];
	TIM3->CCR2 = pwm_linear[0][loc_temp_1.color.r];
	TIM3->CCR4 = pwm_linear[0][loc_temp_1.color.b];
	switch(counter % 60)
	{
		case 30: GPIOA->BSRR = 1<<5; break;
		case 0: GPIOA->BSRR = 1<<(5+16); break;
		default: break;
	}
}
uint16_t get_random_h(void)
{
	uint32_t loc_temp;
	for (uint8_t i = 0; i < 8; i++)
	{
		ADC1->CR |= ADC_CR_ADSTART;
		while ((ADC1->ISR & ADC_ISR_EOC) == 0){};
		CRC->DR = ADC1->DR;
		ADC1->ISR |= ADC_ISR_EOC;
	}
	CRC->DR = 0xBADA55E5;
	delay(4);
	loc_temp = CRC->DR;
	return (uint16_t)(loc_temp % 384);
}
void operation_button(void)
{
	switch(type_of_operation)
	{
		case random_color_fade:
			global_hsv.color.h = get_random_h();
			global_hsv.color.v = 0;
		break;
		case polar_light: global_hsv.color.h = 150; break;
		case flame: global_hsv.color.h = 0; break;
		default: break;
	}
	global_hsv.color.s = 255;
}
void HSV_to_RGB_test(hsv_t* hsv, rgb_888_t* rgb)
{
	uint32_t temp_loc_2;
	uint8_t temp_loc[3];
	if(hsv->color.h > 384) hsv->color.h = 0;
	if(hsv->color.s == 0)
	{
		rgb->color.r = hsv->color.v;
		rgb->color.g = hsv->color.v;
		rgb->color.b = hsv->color.v;
	}
	else
	{
		temp_loc[0] = hsv->color.v;
		temp_loc[1] = (hsv->color.v*(255-hsv->color.s))>>8;
		temp_loc[2] = ((hsv->color.v - temp_loc[1]) * (hsv->color.h % 64)) >> 6;
		switch (hsv->color.h >> 6)
		{
			case 0:
				rgb->color.r = temp_loc[0];
				rgb->color.g = temp_loc[1] + temp_loc[2];
				rgb->color.b = temp_loc[1];
			break;
			case 1:
				rgb->color.r = temp_loc[0] - temp_loc[2];
				rgb->color.g = temp_loc[0];
				rgb->color.b = temp_loc[1];
			break;
			case 2:
				rgb->color.r = temp_loc[1];
				rgb->color.g = temp_loc[0];
				rgb->color.b = temp_loc[1] + temp_loc[2];
			break;
			case 3:
				rgb->color.r = temp_loc[1];
				rgb->color.g = temp_loc[0] - temp_loc[2];
				rgb->color.b = temp_loc[0];
			break;
			case 4:
				rgb->color.r = temp_loc[1] + temp_loc[2];
				rgb->color.g = temp_loc[1];
				rgb->color.b = temp_loc[0];
			break;
			case 5:
				rgb->color.r = temp_loc[0];
				rgb->color.g = temp_loc[1];
				rgb->color.b = temp_loc[0] - temp_loc[2];
			break;
			default: break;
		}
//BRIGHTNESS CORRECTION
		temp_loc_2 = rgb->color.r+rgb->color.g+rgb->color.b;
		rgb->color.r = rgb->color.r*temp_loc[0]/temp_loc_2;
		rgb->color.g = rgb->color.g*temp_loc[0]/temp_loc_2;
		rgb->color.b = rgb->color.b*temp_loc[0]/temp_loc_2;
	}
}
