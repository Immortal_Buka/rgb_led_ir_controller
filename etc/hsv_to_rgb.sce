h = 0:1:383;
s = 255;
v = 255;
r = zeros(length(h));
g = zeros(length(h));
b = zeros(length(h));
su = zeros(length(h));
for i=1:1:384
    t1 = ((255-s)*v)/256;
    t2 = (v-t1)*(modulo(h(i),64))/64;
    t3 = h(i)/64;
    t(i) = t3;
    if (t3 < 1) then 
        r(i) = v;
        g(i) = t1+t2;
        b(i) = t1;
    elseif (t3 < 2) then
        r(i) = v-t2;
        g(i) = v;
        b(i) = t1;
    elseif (t3 < 3) then
        r(i) = t1;
        g(i) = v;
        b(i) = t1+t2;    
    elseif (t3 < 4) then
        r(i) = t1;
        g(i) = v-t2;
        b(i) = v;           
    elseif (t3 < 5) then
        r(i) = t1+t2;
        g(i) = t1;
        b(i) = v;
    else
        r(i) = v;
        g(i) = t1;
        b(i) = v-t2;       
    end
    t1=r(i)+g(i)+b(i);
    r(i) = r(i)*v/t1;
    g(i) = g(i)*v/t1;
    b(i) = b(i)*v/t1;
end
su = (r+g+b);
plot(h, [b g r su]);
xgrid();
