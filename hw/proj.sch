EESchema Schematic File Version 4
LIBS:proj-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1250 4200 1100 4200
Wire Wire Line
	1100 4200 1100 3350
$Comp
L power:+3V3 #PWR01
U 1 1 5C3C6171
P 1100 3250
F 0 "#PWR01" H 1100 3100 50  0001 C CNN
F 1 "+3V3" H 1115 3423 50  0000 C CNN
F 2 "" H 1100 3250 50  0001 C CNN
F 3 "" H 1100 3250 50  0001 C CNN
	1    1100 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3350 1100 3250
Connection ~ 1100 3350
$Comp
L power:GND #PWR02
U 1 1 5C3C61C3
P 2800 4950
F 0 "#PWR02" H 2800 4700 50  0001 C CNN
F 1 "GND" H 2805 4777 50  0000 C CNN
F 2 "" H 2800 4950 50  0001 C CNN
F 3 "" H 2800 4950 50  0001 C CNN
	1    2800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4300 2800 4300
$Comp
L Interface_Optical:TSOP312xx U2
U 1 1 5C3C623F
P 5150 4225
F 0 "U2" H 5137 4650 50  0000 C CNN
F 1 "TSOP312xx" H 5137 4559 50  0000 C CNN
F 2 "OptoDevice:Vishay_CAST-3Pin" H 5100 3850 50  0001 C CNN
F 3 "http://www.vishay.com/docs/82492/tsop312.pdf" H 5800 4525 50  0001 C CNN
	1    5150 4225
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5C3C8A93
P 6000 1300
F 0 "C3" H 6115 1346 50  0000 L CNN
F 1 "C" H 6115 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6038 1150 50  0001 C CNN
F 3 "~" H 6000 1300 50  0001 C CNN
	1    6000 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1100 4900 1100
Wire Wire Line
	4750 1300 4650 1300
$Comp
L Device:C C2
U 1 1 5C3CC3CF
P 4900 1300
F 0 "C2" H 5015 1346 50  0000 L CNN
F 1 "C" H 5015 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4938 1150 50  0001 C CNN
F 3 "~" H 4900 1300 50  0001 C CNN
	1    4900 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1150 4900 1100
Connection ~ 4900 1100
Text GLabel 2850 4100 2    50   Input ~ 0
IR
Text GLabel 5600 4225 2    50   Input ~ 0
IR
Wire Wire Line
	5550 4225 5600 4225
$Comp
L power:GND #PWR011
U 1 1 5C438EA9
P 5650 4525
F 0 "#PWR011" H 5650 4275 50  0001 C CNN
F 1 "GND" H 5655 4352 50  0000 C CNN
F 2 "" H 5650 4525 50  0001 C CNN
F 3 "" H 5650 4525 50  0001 C CNN
	1    5650 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4425 5650 4425
Wire Wire Line
	5650 4425 5650 4525
$Comp
L power:+3V3 #PWR010
U 1 1 5C446A0C
P 5650 3925
F 0 "#PWR010" H 5650 3775 50  0001 C CNN
F 1 "+3V3" H 5665 4098 50  0000 C CNN
F 2 "" H 5650 3925 50  0001 C CNN
F 3 "" H 5650 3925 50  0001 C CNN
	1    5650 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4025 5650 4025
Wire Wire Line
	5650 4025 5650 3925
Wire Wire Line
	4900 1000 4900 1100
Connection ~ 6000 1100
$Comp
L power:GND #PWR012
U 1 1 5C461D72
P 5425 1525
F 0 "#PWR012" H 5425 1275 50  0001 C CNN
F 1 "GND" H 5430 1352 50  0000 C CNN
F 2 "" H 5425 1525 50  0001 C CNN
F 3 "" H 5425 1525 50  0001 C CNN
	1    5425 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3350 2800 3350
Text GLabel 1050 4100 0    50   Input ~ 0
nRES
Text GLabel 2850 3900 2    50   Input ~ 0
SWDIO
Wire Wire Line
	2800 4200 2750 4200
Wire Wire Line
	2800 3350 2800 4200
Wire Wire Line
	2750 3900 2850 3900
Text GLabel 2850 3800 2    50   Input ~ 0
SWDCLK
Wire Wire Line
	2750 3800 2850 3800
Text GLabel 2850 4600 2    50   Input ~ 0
PWM1
Text GLabel 2850 4500 2    50   Input ~ 0
PWM2
Text GLabel 2850 4400 2    50   Input ~ 0
PWM3
$Comp
L power:+3V3 #PWR014
U 1 1 5C3F4A09
P 6525 1000
F 0 "#PWR014" H 6525 850 50  0001 C CNN
F 1 "+3V3" H 6540 1173 50  0000 C CNN
F 2 "" H 6525 1000 50  0001 C CNN
F 3 "" H 6525 1000 50  0001 C CNN
	1    6525 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 4100 1250 4100
Wire Wire Line
	2750 4400 2850 4400
Wire Wire Line
	2750 4500 2850 4500
Wire Wire Line
	2750 4600 2850 4600
Wire Notes Line
	5800 3675 5800 4775
Wire Notes Line
	5800 4775 4600 4775
Wire Notes Line
	4600 3675 4600 4775
Wire Notes Line
	4600 3675 5800 3675
Wire Wire Line
	2800 4300 2800 4900
Wire Wire Line
	1250 3800 1150 3800
Wire Wire Line
	1150 3800 1150 4900
Wire Wire Line
	1150 4900 2800 4900
Connection ~ 2800 4900
Wire Wire Line
	2800 4900 2800 4950
Wire Notes Line
	700  5200 700  2950
$Comp
L Device:R R5
U 1 1 5C56463A
P 1850 2200
F 0 "R5" H 1780 2200 50  0000 R CNN
F 1 "R" H 1780 2245 50  0001 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1780 2200 50  0001 C CNN
F 3 "~" H 1850 2200 50  0001 C CNN
	1    1850 2200
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5C564641
P 2100 2200
F 0 "R6" H 2030 2200 50  0000 R CNN
F 1 "R" H 2030 2245 50  0001 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2030 2200 50  0001 C CNN
F 3 "~" H 2100 2200 50  0001 C CNN
	1    2100 2200
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 5C564648
P 1600 2200
F 0 "R4" H 1530 2200 50  0000 R CNN
F 1 "R" H 1530 2245 50  0001 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 2200 50  0001 C CNN
F 3 "~" H 1600 2200 50  0001 C CNN
	1    1600 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 2350 1600 2400
Wire Wire Line
	1600 2400 1850 2400
Wire Wire Line
	3300 2400 3300 2200
Wire Wire Line
	2900 1900 2900 2400
Connection ~ 2900 2400
Wire Wire Line
	2900 2400 3300 2400
Wire Wire Line
	2100 2350 2100 2400
Connection ~ 2100 2400
Wire Wire Line
	2100 2400 2500 2400
Wire Wire Line
	1850 2350 1850 2400
Connection ~ 1850 2400
Wire Wire Line
	1850 2400 2100 2400
Wire Wire Line
	3000 2000 2100 2000
Wire Wire Line
	2100 2000 2100 2050
Wire Wire Line
	1850 2050 1850 1700
Wire Wire Line
	1850 1700 2600 1700
Wire Wire Line
	1600 2050 1600 1400
Wire Wire Line
	1600 1400 2200 1400
Wire Wire Line
	2500 1600 2500 2400
Connection ~ 2500 2400
Wire Wire Line
	2500 2400 2900 2400
Wire Wire Line
	3300 1800 3300 1350
Wire Wire Line
	2900 1500 2900 1250
Wire Wire Line
	2500 1200 2500 1150
$Comp
L power:GND #PWR04
U 1 1 5C564677
P 3300 2500
F 0 "#PWR04" H 3300 2250 50  0001 C CNN
F 1 "GND" H 3305 2327 50  0000 C CNN
F 2 "" H 3300 2500 50  0001 C CNN
F 3 "" H 3300 2500 50  0001 C CNN
	1    3300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2500 3300 2400
Connection ~ 3300 2400
Wire Wire Line
	3300 1050 3300 950 
Text GLabel 1050 1400 0    50   Input ~ 0
PWM1
Text GLabel 1050 1700 0    50   Input ~ 0
PWM2
Text GLabel 1050 2000 0    50   Input ~ 0
PWM3
Wire Wire Line
	1450 1400 1600 1400
Connection ~ 1600 1400
Wire Wire Line
	1450 1700 1850 1700
Connection ~ 1850 1700
Wire Wire Line
	1450 2000 2100 2000
Connection ~ 2100 2000
$Comp
L Device:R R3
U 1 1 5C56469F
P 1300 2000
F 0 "R3" V 1300 2000 50  0000 C CNN
F 1 "R" V 1184 2000 50  0001 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1230 2000 50  0001 C CNN
F 3 "~" H 1300 2000 50  0001 C CNN
	1    1300 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5C5646A6
P 1300 1700
F 0 "R2" V 1300 1700 50  0000 C CNN
F 1 "R" V 1184 1700 50  0001 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1230 1700 50  0001 C CNN
F 3 "~" H 1300 1700 50  0001 C CNN
	1    1300 1700
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5C5646AD
P 1300 1400
F 0 "R1" V 1300 1400 50  0000 C CNN
F 1 "R" V 1184 1400 50  0001 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1230 1400 50  0001 C CNN
F 3 "~" H 1300 1400 50  0001 C CNN
	1    1300 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	1050 1400 1150 1400
Wire Wire Line
	1050 1700 1150 1700
Wire Wire Line
	1050 2000 1150 2000
Wire Notes Line
	700  700  700  2750
$Comp
L Connector:Barrel_Jack_MountingPin J3
U 1 1 5C3F6102
P 4350 1200
F 0 "J3" H 4405 1517 50  0000 C CNN
F 1 "Barrel_Jack_MountingPin" H 4550 750 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 4400 1160 50  0001 C CNN
F 3 "~" H 4400 1160 50  0001 C CNN
	1    4350 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q1
U 1 1 5C40CDCA
P 2400 1400
F 0 "Q1" H 2300 1250 50  0000 L CNN
F 1 "Q_NMOS_GSD" V 2650 1150 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2600 1500 50  0001 C CNN
F 3 "~" H 2400 1400 50  0001 C CNN
	1    2400 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q2
U 1 1 5C40CE52
P 2800 1700
F 0 "Q2" H 2700 1550 50  0000 L CNN
F 1 "Q_NMOS_GSD" V 3050 1450 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3000 1800 50  0001 C CNN
F 3 "~" H 2800 1700 50  0001 C CNN
	1    2800 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q3
U 1 1 5C40CF55
P 3200 2000
F 0 "Q3" H 3100 1850 50  0000 L CNN
F 1 "Q_NMOS_GSD" V 3450 1750 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3400 2100 50  0001 C CNN
F 3 "~" H 3200 2000 50  0001 C CNN
	1    3200 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4100 2850 4100
$Comp
L power:VDD #PWR013
U 1 1 5C499F5F
P 6000 1000
F 0 "#PWR013" H 6000 850 50  0001 C CNN
F 1 "VDD" H 6017 1173 50  0000 C CNN
F 2 "" H 6000 1000 50  0001 C CNN
F 3 "" H 6000 1000 50  0001 C CNN
	1    6000 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5C5B1023
P 3250 4250
F 0 "C1" H 3365 4296 50  0000 L CNN
F 1 "C" H 3365 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3288 4100 50  0001 C CNN
F 3 "~" H 3250 4250 50  0001 C CNN
	1    3250 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3350 3250 3350
Wire Wire Line
	3250 3350 3250 4100
Connection ~ 2800 3350
Wire Wire Line
	2800 4900 3250 4900
Wire Wire Line
	3250 4900 3250 4400
Wire Notes Line
	3500 5200 3500 2950
Wire Notes Line
	700  5200 3500 5200
Wire Notes Line
	700  2950 3500 2950
$Comp
L Device:D_Schottky D1
U 1 1 5C73E6B8
P 6225 1100
F 0 "D1" H 6225 884 50  0000 C CNN
F 1 "D_Schottky" H 6225 975 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123F" H 6225 1100 50  0001 C CNN
F 3 "~" H 6225 1100 50  0001 C CNN
	1    6225 1100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J4
U 1 1 5C76CEAA
P 5050 2950
F 0 "J4" H 5100 3367 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5100 3276 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Vertical" H 5050 2950 50  0001 C CNN
F 3 "~" H 5050 2950 50  0001 C CNN
	1    5050 2950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR07
U 1 1 5C76CFFB
P 4750 2650
F 0 "#PWR07" H 4750 2500 50  0001 C CNN
F 1 "+3V3" H 4765 2823 50  0000 C CNN
F 2 "" H 4750 2650 50  0001 C CNN
F 3 "" H 4750 2650 50  0001 C CNN
	1    4750 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2650 4750 2750
Wire Wire Line
	4750 2750 4850 2750
Text GLabel 5400 2750 2    50   Input ~ 0
SWDIO
Wire Wire Line
	5350 2750 5400 2750
Text GLabel 5400 2850 2    50   Input ~ 0
SWDCLK
Wire Wire Line
	5350 2850 5400 2850
$Comp
L power:GND #PWR08
U 1 1 5C777209
P 4750 3250
F 0 "#PWR08" H 4750 3000 50  0001 C CNN
F 1 "GND" H 4755 3077 50  0000 C CNN
F 2 "" H 4750 3250 50  0001 C CNN
F 3 "" H 4750 3250 50  0001 C CNN
	1    4750 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3250 4750 3150
Wire Wire Line
	4750 2850 4850 2850
Wire Wire Line
	4850 2950 4750 2950
Connection ~ 4750 2950
Wire Wire Line
	4750 2950 4750 2850
Wire Wire Line
	4850 3050 4750 3050
Connection ~ 4750 3050
Wire Wire Line
	4750 3050 4750 2950
Wire Wire Line
	4850 3150 4750 3150
Connection ~ 4750 3150
Wire Wire Line
	4750 3150 4750 3050
Text GLabel 5400 3150 2    50   Input ~ 0
nRES
Wire Wire Line
	5350 3150 5400 3150
Wire Wire Line
	6000 1100 6000 1000
Wire Wire Line
	6000 1100 6075 1100
Wire Wire Line
	6375 1100 6525 1100
Wire Wire Line
	6525 1100 6525 1000
Wire Notes Line
	4600 2400 5800 2400
Wire Notes Line
	5800 2400 5800 3500
Wire Notes Line
	5800 3500 4600 3500
Wire Notes Line
	4600 2400 4600 3500
Wire Wire Line
	6000 1450 6000 1475
Wire Wire Line
	4750 1300 4750 1475
Wire Wire Line
	4900 1450 4900 1475
Wire Notes Line
	4050 700  6675 700 
$Comp
L power:GND #PWR05
U 1 1 5C81FFC3
P 3450 1525
F 0 "#PWR05" H 3450 1275 50  0001 C CNN
F 1 "GND" H 3455 1352 50  0000 C CNN
F 2 "" H 3450 1525 50  0001 C CNN
F 3 "" H 3450 1525 50  0001 C CNN
	1    3450 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1050 3525 1050
Wire Wire Line
	2500 1150 3525 1150
Wire Wire Line
	2900 1250 3525 1250
Wire Wire Line
	3300 1350 3525 1350
$Comp
L Connector_Generic:Conn_01x05 J1
U 1 1 5C82D825
P 3725 1150
F 0 "J1" H 3700 1450 50  0000 L CNN
F 1 "Conn_01x05" V 3850 925 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 3725 1150 50  0001 C CNN
F 3 "~" H 3725 1150 50  0001 C CNN
	1    3725 1150
	1    0    0    -1  
$EndComp
Wire Notes Line
	3925 2750 3925 700 
Wire Notes Line
	700  2750 3925 2750
Wire Notes Line
	700  700  3925 700 
Wire Wire Line
	3450 950  3525 950 
Wire Wire Line
	3450 950  3450 1525
$Comp
L power:VAA #PWR03
U 1 1 5C76E73F
P 3300 950
F 0 "#PWR03" H 3300 800 50  0001 C CNN
F 1 "VAA" H 3317 1123 50  0000 C CNN
F 2 "" H 3300 950 50  0001 C CNN
F 3 "" H 3300 950 50  0001 C CNN
	1    3300 950 
	1    0    0    -1  
$EndComp
$Comp
L power:VAA #PWR09
U 1 1 5C77264C
P 4900 1000
F 0 "#PWR09" H 4900 850 50  0001 C CNN
F 1 "VAA" H 4917 1173 50  0000 C CNN
F 2 "" H 4900 1000 50  0001 C CNN
F 3 "" H 4900 1000 50  0001 C CNN
	1    4900 1000
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AMS1117-3.3 U3
U 1 1 5CA36549
P 5425 1100
F 0 "U3" H 5425 1342 50  0000 C CNN
F 1 "AMS1117-3.3" H 5425 1251 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5425 1300 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 5525 850 50  0001 C CNN
	1    5425 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1100 5125 1100
Wire Wire Line
	5725 1100 6000 1100
Wire Wire Line
	6000 1100 6000 1150
Wire Wire Line
	4900 1475 5425 1475
Wire Wire Line
	5425 1475 5425 1525
Connection ~ 4900 1475
Wire Wire Line
	5425 1475 5425 1400
Connection ~ 5425 1475
Wire Wire Line
	4900 1475 4750 1475
Wire Wire Line
	5425 1475 6000 1475
Wire Notes Line
	4050 700  4050 1775
Wire Notes Line
	6675 700  6675 1775
Wire Notes Line
	4050 1775 6675 1775
$Comp
L schematic:STM32F030F4P6 U1
U 1 1 5D63C373
P 2000 4250
F 0 "U1" H 2000 4965 50  0000 C CNN
F 1 "STM32F030F4P6" H 2000 4874 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 2000 4250 50  0001 C CNN
F 3 "DOCUMENTATION" H 2000 4250 50  0001 C CNN
	1    2000 4250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
